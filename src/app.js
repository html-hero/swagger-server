
const fs = require('fs');
const http = require('http');
const path = require('path');
const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const send = require('koa-send');
const auth = require('koa-basic-auth');

const app = new Koa();
app.use(bodyParser());
app.use(auth({
  name: process.env.AUTH_NAME || 'admin',
  pass: process.env.AUTH_PASS || 'P@ssw0rd!',
}));

const fileFinder = (ext, locations) => {
  const docPath = path.join(__dirname, ...locations);
  const docs = fs.readdirSync(docPath);
  const htmls = docs.map(d => {
    if (fs.lstatSync(path.join(docPath, d)).isFile()) {
      return { name: d.replace(ext, ''), file: d, location: path.join(docPath, d) };
    }
  }).filter(Boolean);

  return htmls;
};

const router = new Router();
router.get('/', async (ctx, next) => {
  const { svc } = ctx.query;
  const files = fileFinder('.html', ['..', 'docs']);
  if (!files.map(h => h.name).includes(svc)) {
    ctx.status = 400;
    return next();
  }

  const content = files.filter(h => h.name === svc)[0];

  await send(ctx, path.join('docs', content.file));
});

router.get('/docs/swagger/:filename', async (ctx, next) => {
  const { filename } = ctx.params;
  const files = fileFinder('.yml', ['..', 'docs', 'swagger']);
  if (!files.map(h => h.file).includes(filename)) {
    ctx.status = 400;
    return next();
  }

  const content = files.filter(h => h.file === filename)[0];

  await send(ctx, path.join('docs', 'swagger', content.file));
});

app.use(router.routes());
const server = http.createServer(app.callback());
module.exports = { server };


