
const _ = require('lodash');
const { createLogger, format, transports } = require('winston');

const formatter = (logFormat) => {
  switch (logFormat) {
    case 'json':
      return format.json();
    case 'simple':
      return format.simple();
    default:
      return format.simple();
  }
};

const isSilent = silent => (_.isEmpty(silent) ? true : _.toUpper(silent) === 'TRUE');

const log = createLogger({
  level: 'debug',
  silent: isSilent(process.env.LOG_SILENT),
  format: formatter(process.env.LOG_FORMAT),
  transports: [new transports.Console()],
});

const overrideStream = () => {
  log.stream = {
    write: (message) => {
      log.info(message);
    },
  };
};

module.exports = {
  log,
  formatter,
  isSilent,
  overrideStream,
};
