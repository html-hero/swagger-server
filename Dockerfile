FROM node:10.15.3-alpine

RUN mkdir /app

WORKDIR /app

COPY package.json package-lock.json ./

RUN npm install

COPY . .

ENTRYPOINT [ "sh", "-c", "bin/web" ]
